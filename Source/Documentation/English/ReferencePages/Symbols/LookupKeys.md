{
"More Information" -> {
    "LookupKeys[l] is equivalent to LookupKeys[l,1]"
  },

  "Basic Examples" -> {
    "rules1 = {a->1,b->2,c->3}",
    "lu1=ToLookupTable[rules1]",
    "LookupKeys[lu1]",
    "rules2 = {a->{aa->1,bb->2},b->2,c->3}",
    "lu2 = ToLookupTable[rules2,2]",
    "LookupKeys[lu2,2]"
},
  "See Also" ->
    {"ToLookupRules","ToLookupTable","Lookup","LookupKeyQ","SetLookupRule","DeleteLookupKey"}

}
