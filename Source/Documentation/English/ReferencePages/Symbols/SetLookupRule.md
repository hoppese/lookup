{
"More Information" -> {
  },

  "Basic Examples" -> {
    "rules = {a->1,b->2,c->3}",
    "lu = ToLookupTable[rules]",
    "lu2 = SetLookupRule[lu,c,4]",
    "ToLookupRules[lu2]",
    "lu3 = SetLookupRule[lu2,d,e,5]",
    "ToLookupRules[lu3,2]"
},
  "See Also" -> {"ToLookupTable","ToLookupRules","LookupKeys","Lookup","LookupKeyQ","DeleteLookupKey"}

}
