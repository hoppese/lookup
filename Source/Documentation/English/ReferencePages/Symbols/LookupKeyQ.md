{
"More Information" -> {
  },

  "Basic Examples" -> {
    "rules = {a->{aa->1,bb->2},b->2,c->3}",
    "lu2 = ToLookupTable[rules,2]",
    "LookupKeyQ[lu2,a,aa]",
    "LookupKeyQ[lu2,d]"
},
  "See Also" ->
    {"ToLookupRules","ToLookupTable","Lookup","LookupKeys","SetLookupRule","DeleteLookupKey"}

}
