{
"More Information" -> {
    "ToLookupRules[l] is equivalent to ToLookupRules[l, 1]",
    "Use ToLookupTable[l, Infinity] to convert arbitrarily deeply nest LookupTables to rules"
  },

  "Basic Examples" -> {
    "rules = {a->{aa->1,bb->2},b->2,c->3}",
    "lu = ToLookupTable[rules,2]",
    "ToLookupRules[lu]",
    "ToLookupRules[lu,2]"
},
  "See Also" -> {"ToLookupTable","LookupKeys","Lookup","LookupKeyQ","SetLookupRule","DeleteLookupKey"}

}
