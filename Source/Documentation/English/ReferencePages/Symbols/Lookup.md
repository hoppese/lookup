{
"More Information" -> {
    "Lookup[Lookup[l,k1],k2] etc. is equivalent to Lookup[l,k1,k2, ...]",
    "Lookup[l,k1,k2, ...] is equivalent to l[k1,k2, ...]"
  },

  "Basic Examples" -> {
    "rules1 = {a->1,b->2,c->3}",
    "lu1=ToLookupTable[rules1]",
    "Lookup[lu1,a]",
    "rules2 = {a->{aa->1,bb->2},b->2,c->3}",
    "lu2 = ToLookupTable[rules2,2]",
    "Lookup[lu2,a,aa]"
},
  "See Also" -> {"ToLookupRules","LookupKeys","ToLookupTable","LookupKeyQ","SetLookupRule","DeleteLookupKey"}
}
