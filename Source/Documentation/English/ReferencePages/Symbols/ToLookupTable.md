{
"More Information" -> {
    "ToLookupTable[l] is equivalent to ToLookupTable[l, 1]",
    "Use ToLookupTable[l, Infinity] to nest create a nested LookupTable to arbitrary depth"
  },

  "Basic Examples" -> {
    "rules1 = {a->1,b->2,c->3}",
    "lu1=ToLookupTable[rules1]",
    "Lookup[lu1,a]",
    "rules2 = {a->{aa->1,bb->2},b->2,c->3}",
    "lu2 = ToLookupTable[rules2,2]",
    "Lookup[lu2,a,aa]"
},
  "See Also" -> {"ToLookupRules","LookupKeys","Lookup","LookupKeyQ","SetLookupRule","DeleteLookupKey"}

}
