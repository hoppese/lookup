{
"More Information" -> {
  },

  "Basic Examples" -> {
    "rules = {a->1,b->2,c->3}",
    "lu = ToLookupTable[rules]",
    "lu2 = DeleteLookupKey[lu,c]",
    "ToLookupRules[lu2]"
    },
  "See Also" -> {"ToLookupTable","ToLookupRules","LookupKeys","Lookup","LookupKeyQ","SetLookupRules"}

}
