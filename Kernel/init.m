(* ::Package:: *)

(* Mathematica Init File *)

Get["Lookup`Kernel`Lookup`"];


Block[{},
	Unprotect[$Packages];
	$Packages = Complement[$Packages, Lookup`Private`packages];
	Protect[$Packages];

	Scan[Needs, Lookup`Private`packages];
]
