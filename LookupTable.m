(* ::Package:: *)

BeginPackage["Lookup`LookupTable`"];


LookupValue::usage="LookupValue[l,k] returns the value corresponding to key k from first level of LookupTable l.\n\
LookupValue[l,k1,k2,...] returns returns the value corresponding to the key sequence k1,k2,... in LookupTable l.\n\
Each subsequent key in the sequence represents a key at the next lower level";
(*Lookup::usage="Lookup[l,k] returns the value corresponding to key k from first level of LookupTable l.\n \
Lookup[l,k1,k2,...] returns returns the value corresponding to the key sequence k1,k2,... in LookupTable l.\n\
Each subsequent key in the sequence represents a key at the next lower level";
*)
LookupTable::usage="LookupTable objects store potentially nested key-value pairs. They are created with ToLookupTable and \
can be accessed and manipulated with LookupValue, LookupKeys, LookupKeyQ, ToLookupRules, SetLookupRule, DeleteLookupKey.";
ToLookupTable::usage="ToLookupTable[l] makes a LookupTable with out of the key-value pairs given as rules in the first level of List l.\n\
ToLookupTable[l,levelspec] makes a potentially nested LookupTable with the nesting down to level levelspec.";
ToLookupRules::usage="ToLookupRules[l] returns the first level of LookupTable l as a list of key-value pair rules.\n\
ToLookupRules[l,levelspec] returns a potentially nested list of rules with the nesting down to level levelspec.";
LookupKeys::usage="LookupKeys[l] returns the keys from first level of LookupTable l as a list.\n\
LookupKeys[l,levelspec] returns a potentially nested list of keys with the nesting down to level levelspec.";
LookupKeyQ::usage="LookupKeyQ[l,k] returns True if the key k is present at level 1 in LookupTable l and False otherwise.\n\
LookupKeyQ[l,k1,k2,...] returns True if the key Sequence k1,k2,... is present in LookupTable l and False otherwise. \
Each subsequent key in the sequence represents a key at the next lower level.";
DeleteLookupKey::usage="DeleteLookupKey[l,k] deletes key k from level 1 of LookupTable l.\n\
DeleteLookupKey[l,k1,k2,...] deletes the key Sequence k1,k2,... from LookupTable l. \
Each subsequent key in the sequence represents a key at the next lower level.";
SetLookupRule::usage="SetLookupRule[l,k,v] sets the value of key k to v at level 1 of LookupTable l. If key k is not present, it is added with value v.\n\
SetLookupRule[l,k1,k2,...,v] sets the value of the key Sequence k1,k2,... to v in LookupTable l. \
Each subsequent key in the sequence represents a key at the next lower level. \
If the sequence is not present, it is added with value v.";

(* Experimental *)
JoinLookupTables::usage="JoinLookupTables[l1,l2] joins the two LookupTables l1 and l2 at the first level.\n \
JoinLookupTables[l1,k1,...,l2] joins the two LookupTables l1 and l2, with l2 nested at the location specified \
by the key sequence k1,... Each subsequent key in the sequence represents a key at the next lower level.";

LookupRuleQ::usage="LookupRuleQ[l,k,v] returns True if the key k is present at level 1 in LookupTable l and its value is v. It returns False otherwise.\n\
LookupRuleQ[l,k1,k2,...,v] returns True if the key Sequence k1,k2,... is present in LookupTable l and its value is v. It returns False otherwise. \
Each subsequent key in the sequence represents a key at the next lower level.";
LookupMatchQ::usage="LookupMatchQ[l,k,p] returns True if the key k is present at level 1 in LookupTable l and its value matches pattern p. It returns False otherwise.\n\
LookupMatchQ[l,k1,k2,...,p] returns True if the key Sequence k1,k2,... is present in LookupTable l and its value matches pattern p. It returns False otherwise. \
Each subsequent key in the sequence represents a key at the next lower level.";
ActOnLeaves::usage="ActOnLeaves[l,func] Acts with the function func on each leaf of the (potentially nested) LookupTable l. \
It returns l with each leaf replaced by func[leaf].";


Begin["`Private`"];


LookupTable/:Format[l_LookupTable]:="<< LookupTable, "<> ToString@Length[l]<>" >>"


ToLookupTable[l:{HoldPattern[_->_]...}]:=ToLookupTable[l,1]


ToLookupTable[l:{HoldPattern[_->_]...},Infinity]:=
Module[{lUsed},
	lUsed=If[MatchQ[#,_->{HoldPattern[_->_]...}],
				#[[1]]->ToLookupTable[#[[2]],Infinity],
				#
			]&/@l;
	makeLookupTable[lUsed]
]


ToLookupTable[l:{HoldPattern[_->_]...},level_Integer]/;level>=1:=
Module[{lUsed},
	lUsed=If[MatchQ[#,_->{HoldPattern[_->_]...}&&level>1],
				#[[1]]->ToLookupTable[#[[2]],level-1],
				#
			]&/@l;

	makeLookupTable[lUsed]
]


(* This is because Dispatch doesn't seem to work for lists smaller than 3 *)
(*makeLookupTable[l:{HoldPattern[_->_]...}]:=If[Length@l>3,LookupTable[Dispatch[l]],LookupTable[{l}]]*)
makeLookupTable[l:{HoldPattern[_->_]...}]:=LookupTable[l]


LookupTable/:ToLookupRules[l_LookupTable]:=ToLookupRules[l,1]


LookupTable/:ToLookupRules[l_LookupTable,Infinity]:=
Module[{rules},
	rules=ToLookupRules[l];
	rules//.l1_LookupTable:>ToLookupRules[l1]
]


LookupTable/:ToLookupRules[l_LookupTable,depth_Integer/;depth>=1]:=
Module[{rules},
	rules=l[[1]];
	If[depth==1,
		rules,
		rules/.l1_LookupTable:>ToLookupRules[l1,depth-1]
	]
]


LookupTable/:LookupKeys[l_LookupTable]:=ToLookupRules[l][[All,1]]


LookupTable/:LookupKeys[l_LookupTable,depth_/;depth>=1]:=subKeys[l,#,depth]&/@LookupKeys[l]


LookupTable/:LookupKeys[l_LookupTable,Infinity]:=subKeys[l,#,Infinity]&/@LookupKeys[l]


LookupTable/:subKeys[l_LookupTable,key_,Infinity]:=
Module[{value},
	value=LookupValue[l,key];
	If[MatchQ[value,_LookupTable],
		key->(subKeys[value,#,Infinity]&/@LookupKeys[value]),
		key
	]
]


LookupTable/:subKeys[l_LookupTable,key_,depth_/;depth>=1]:=
Module[{value},
	value=LookupValue[l,key];
	If[MatchQ[value,_LookupTable]&&depth>1,
		key->(subKeys[value,#,depth-1]&/@LookupKeys[value]),
		key
	]
]


(* This is because Dispatch doesn't seem to work for lists smaller than 3 *)
(*LookupTable/:replacements[l_LookupTable]:=If[Length@l>3,l[[1]],l[[1,1]]]*)
LookupTable/:replacements[l_LookupTable]:=l[[1]]


LookupTable/:LookupValue[l_LookupTable,key_]:=
If[LookupKeyQ[l,key],
	key/.replacements[l],
	Print["Key ", key, " not in list of keys: ", LookupKeys[l]];
	Abort[]
];



LookupTable/:LookupValue[l_LookupTable,key_,moreKeys:HoldPattern[_]..]:=
Module[{value},

	value=LookupValue[l,key];

	If[MatchQ[value,_LookupTable],
		LookupValue[value,moreKeys],
		Print["Number of keys given exceeds depth of LookupTables"];
		Abort[]
	]
]


LookupTable/:LookupValue[l_LookupTable,All]:=ToLookupRules[l][[All,2]]


LookupTable/:l_LookupTable[keys:HoldPattern[_]..]:=LookupValue[l,keys]


LookupTable/:LookupKeyQ[l_LookupTable,key_]:=MemberQ[LookupKeys[l],key]


LookupTable/:LookupKeyQ[l_LookupTable,keys:HoldPattern[_]..]:=
Module[{value},

	If[LookupKeyQ[l,First@{keys}],
		If[Length[{keys}]==1,
			True,
			value=LookupValue[l,First@{keys}];
			If[MatchQ[value,_LookupTable],
				LookupKeyQ[value,Sequence@@(Rest@{keys})],
				False
			]
		],
		False
	]

]


LookupTable/:DeleteLookupKey[l_LookupTable,keys:HoldPattern[_]..]:=
Module[{luNew,subL},

	If[LookupKeyQ[l,keys],
		If[Length@{keys}==1,
			ToLookupTable[DeleteCases[ToLookupRules[l],(Last@{keys}->_)]],
			subL=LookupValue[l,Sequence@@(Most@{keys})];
			luNew=ToLookupTable[DeleteCases[ToLookupRules[subL],(Last@{keys}->_)]];
			ReplaceLookupRule[l,Sequence@@(Most@{keys}),luNew]
		],

		Print["Key sequence ", Sequence@@(ToString[#]<>", "&/@{keys}), "not found in LookupTable."];
		Abort[]
	]
]



LookupTable/:SetLookupRule[l_LookupTable,keys:HoldPattern[_]..,value_]:=
Module[{keySubLists,lNew,k},

	If[LookupKeyQ[l,keys],

		ReplaceLookupRule[l,keys,value],

		(* This is a special case when a subset of the keys match and the leaf is not a LookupTable *)
		keySubLists=Take[{keys},#]&/@Drop[Reverse@Range@Length@{keys},1];
		lNew=l;

		Do[If[LookupKeyQ[l,Sequence@@k]&&!MatchQ[LookupValue[l,Sequence@@k],_LookupTable],		
			lNew=DeleteLookupKey[l,Sequence@@k];
			Break[];
		],{k,keySubLists}];

		AddLookupRule[lNew,keys,value]
	]
]


LookupTable/:AddLookupRule[l_LookupTable,mostKeys:HoldPattern[_]..,lastKey_,value_]:=
Module[{luNew,luParent,luParentNew},

	If[LookupKeyQ[l,mostKeys,lastKey],
			Print["Key sequence ", Sequence@@(ToString[#]<>", "&/@{mostKeys,lastKey}), "already present in LookupTable."];
			Abort[],
		
		If[LookupKeyQ[l,mostKeys],
			luParent=LookupValue[l,mostKeys];
			
			If[MatchQ[luParent,_LookupTable],
				luParentNew=AddLookupRule[luParent,lastKey,value];
				ReplaceLookupRule[l,mostKeys,luParentNew],
				
				Print["Key sequence ", Sequence@@(ToString[#]<>", "&/@{mostKeys}), "already present in LookupTable, and doesn't point to a LookupTable."];
				Abort[]
			],
			
			luNew=ToLookupTable[{lastKey->value}];
			AddLookupRule[l,mostKeys,luNew]
		]	
	]
]


LookupTable/:AddLookupRule[l_LookupTable,key_,value_]:=
If[LookupKeyQ[l,key],
	Print["Key ", key,  " already present in LookupTable."];
	Abort[],
	ToLookupTable[Join[ToLookupRules[l],{key->value}]]
]


LookupTable/:ReplaceLookupRule[l_LookupTable,key_,value_]:=
	If[LookupKeyQ[l,key],
		
		ToLookupTable[Replace[ToLookupRules[l],(key->_):>(key->value),{1}]],
				
		Print["Key ",key, " not found in list of keys ", LookupKeys@l];
		Abort[]
	]


LookupTable/:ReplaceLookupRule[l_LookupTable,mostKeys:HoldPattern[_]..,lastKey_,value_]:=
Module[{luNew,subL},

	If[LookupKeyQ[l,mostKeys,lastKey],
		
			subL=LookupValue[l,mostKeys];
			luNew=ToLookupTable[Replace[ToLookupRules[subL],(lastKey->_):>(lastKey->value),{1}]];
			ReplaceLookupRule[l,mostKeys,luNew]
		,
		
		Print["Key sequence ",Sequence@@(ToString[#]<>", "&/@{mostKeys,lastKey}), "not found in LookupTable."];
		Abort[]
	]
]


LookupTable/:Length[l_LookupTable]:=Length[ToLookupRules[l]]


(* Experimental *)


LookupTable/:JoinLookupTables[l_LookupTable,keys:HoldPattern[_]...,l2_LookupTable]:=
Module[{lNew},
	lNew=l;
	Do[
		lNew=
			If[LookupKeyQ[lNew,keys,k],
				If[LookupMatchQ[lNew,keys,k,_LookupTable]&&LookupMatchQ[l2,k,_LookupTable],
					SetLookupRule[lNew,keys,k,JoinLookupTables[LookupValue[lNew,keys,k],LookupValue[l2,k]]],
					Print["LookupTables with the same key sequence: ", Sequence@@(ToString[#]<>", "&/@{keys,k}), "cannot be joined."];
					Abort[]
				],
				SetLookupRule[lNew,keys,k,LookupValue[l2,k]]],
		{k,LookupKeys[l2]}
	];
	lNew
]


LookupTable/:LookupRuleQ[l_LookupTable,keys:HoldPattern[_]..,value_]:=
If[LookupKeyQ[l,keys],
	LookupValue[l,keys]===value,
	False
]


LookupTable/:LookupMatchQ[l_LookupTable,keys:HoldPattern[_]..,pattern_]:=
If[LookupKeyQ[l,keys],
	MatchQ[LookupValue[l,keys],pattern],
	False
]


LookupTable/:ActOnLeaves[l_LookupTable,fn_]:=
Module[{keys,lNew,elem,k},
	lNew=l;
	keys=LookupKeys[lNew];
	
	Do[elem=LookupValue[lNew,k];
		lNew=SetLookupRule[lNew,k,If[MatchQ[elem,_LookupTable],ActOnLeaves[elem,fn],fn[elem]]],
		{k,keys}
	];
	lNew
]


End[];

EndPackage[];
