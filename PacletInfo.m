(* ::Package:: *)

(* Paclet Info File *)

(* created 2013/05/11*)

Paclet[
    Name -> "Lookup",
    Version -> "0.0.1",
    MathematicaVersion -> "6+",
	Creator->"Seth Hopper",
	Description->"Lookup provides LookupTables for storing and accessing key-value pairs",
    Extensions -> 
        {
			{"Kernel",
				"Context"->{
					"Lookup`",
					"Lookup`LookupTable`"
				}
			},

            {"Documentation", Resources -> {"Guides/Lookup"}, Language -> "English"}
	}
]


